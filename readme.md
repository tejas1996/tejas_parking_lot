**Project Structure**:
.
|----bin
|----src
|----target
|----functional_spec

1. bin contains the two files that would build and run the program(setup and parking_lot).
2. src contains the source files.
3. target contains the compiled files as well as the jar.
4. functional_spec 


**Code Structure**:
1. The program has 3 classes along with a Main class:
    a. ParkingLot: Contains the implementation of the functions of the commands. 
    b. CommandParser: Contains the code for the Parsing and Validation of the commands.
    c. CommandProcessor: Contains the code that calls the corresponding function from the ParkingLot class.
    d. Main: Contains code to take input from the user in both interactive and file method.



**To build the program**:
Execute ./bin/setup
This will build the package and run the unit testcases. 


**To run the program**:
1. Run ./bin/parking_lot.sh <input_filepath> 
The inputs commands are expected and taken from the file specified

2. Run ./bin/parking_lot.sh 
This will start the program in interactive mode.


**Assumptions made**:
1) As the commands are taken through file and interactive mode, there won't be any two concurrent commands.
2) As the commands are not concurrent we are not making the parking lot thread safe.
3) As currently we just have a single car object, I did not make a parent class vehile to avoid complexity and over-engineering.
