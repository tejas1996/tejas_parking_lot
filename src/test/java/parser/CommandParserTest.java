package parser;

import org.junit.Assert;
import org.junit.Test;

public class CommandParserTest {

    CommandParser commandParser = new CommandParser();

    @Test
    public void validateCreateParkingLotTest() {

        String creaateParkingLotCommand = "create_parking_lot 6";
        boolean result = commandParser.validateQuery(creaateParkingLotCommand);
        Assert.assertEquals(true, result);
    }


    @Test
    public void validLeaveQueryTest() {

        String leaveQuery = "leave 5";
        boolean result = commandParser.validateQuery(leaveQuery);
        Assert.assertEquals(true, result);

    }

    @Test
    public void validParkQueryTest() {

        String parkQuery = "park KA-01-HH-1234 White";
        boolean result = commandParser.validateQuery(parkQuery);
        Assert.assertEquals(true, result);

    }

    @Test
    public void validStatusQueryTest() {

        String statusQuery = "status";
        boolean result = commandParser.validateQuery(statusQuery);
        Assert.assertEquals(true, result);

    }


    @Test
    public void validColorToNumberQueryTest() {

        String colorToNumberQuery = "registration_numbers_for_cars_with_colour White";
        boolean result = commandParser.validateQuery(colorToNumberQuery);
        Assert.assertEquals(true, result);

    }

    @Test
    public void validColorToSlotTest() {

        String colorToSlotQuery = "slot_numbers_for_cars_with_colour White";
        boolean result = commandParser.validateQuery(colorToSlotQuery);
        Assert.assertEquals(true, result);

    }

    @Test
    public void validSlotFromNumberTest() {

        String slotFromNumberQuery = "  slot_number_for_registration_number KA-01-HH-3141   ";
        boolean result = commandParser.validateQuery(slotFromNumberQuery);
        Assert.assertEquals(true, result);

    }

    @Test
    public void validateWrongQueriesTest() {

        String wrongCreaateParkingLotCommand = "create_parking_lot 6s";
        boolean result = commandParser.validateQuery(wrongCreaateParkingLotCommand);
        Assert.assertEquals(false, result);

        String wrongParkCommand = "park KA-01-HH-1234 White And Grey";
        boolean resultForPark = commandParser.validateQuery(wrongParkCommand);
        Assert.assertEquals(false, resultForPark);

        String slotFromNumberQuery = "slot_number_for_registration_numberda KA-01-HH-3141";
        boolean resultNumberToQuery = commandParser.validateQuery(slotFromNumberQuery);
        Assert.assertEquals(false, resultNumberToQuery);

        String statusQuery = "give me status";
        boolean resultForStatus = commandParser.validateQuery(statusQuery);
        Assert.assertEquals(false, resultForStatus);

    }


}