package parkinglot;

import models.Car;
import org.junit.Assert;
import org.junit.Test;

public class ParkingLotTest {

    @Test
    public void testPark() {

        ParkingLot parkingLot = new ParkingLot(3);
        Car car = new Car("MH087676" , "White");
        parkingLot.park(car);
        // Checking if one slot is taken out of 3
        Assert.assertEquals(2, parkingLot.openSlots.size());
        Assert.assertEquals(1, parkingLot.parkedSlots.size());
        Assert.assertEquals(1, parkingLot.parkedCars.size());

    }

    @Test
    public void testLeave() {

        ParkingLot parkingLot = new ParkingLot(3);
        Car car = new Car("MH087676" , "White");
        parkingLot.park(car);
        parkingLot.leave("1");
        Assert.assertEquals(3, parkingLot.openSlots.size());

    }



}