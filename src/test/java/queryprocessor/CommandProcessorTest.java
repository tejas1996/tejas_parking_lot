package queryprocessor;

import org.junit.Assert;
import org.junit.Test;

public class CommandProcessorTest {

    CommandProcessor commandProcessor = new CommandProcessor();

    @Test
    public void queryTest() {

        String correctCreaateParkingLotCommand = "create_parking_lot 6";
        String result = commandProcessor.execute(correctCreaateParkingLotCommand);
        Assert.assertEquals("Created a parking lot with 6 slots", result);
        String correctParkCommand = "park KA-01-HH-1234 White";
        String resultForPark = commandProcessor.execute(correctParkCommand);
        Assert.assertEquals("Allocated slot number: 1", resultForPark);

    }
    @Test
    public void testDuplicateParkingLot() {

        String correctCreaateParkingLotCommand = "create_parking_lot 6";
        String result = commandProcessor.execute(correctCreaateParkingLotCommand);
        Assert.assertEquals("Created a parking lot with 6 slots", result);
        String correctCreaateParkingLotCommandAgain = "create_parking_lot 7";
        String resultForSecondQuery = commandProcessor.execute(correctCreaateParkingLotCommandAgain);
        Assert.assertEquals("Parking lot already exits", resultForSecondQuery);

    }

    @Test
    public void testDuplicateParkCars(){
        String createParkingLotCommand = "create_parking_lot 6";
        String result = commandProcessor.execute(createParkingLotCommand);
        Assert.assertEquals("Created a parking lot with 6 slots", result);
        String parkCommand = "park KA-01-HH-1234 White";
        String resultForPark = commandProcessor.execute(parkCommand);
        Assert.assertEquals("Allocated slot number: 1", resultForPark);
        String resultForDuplicatePark = commandProcessor.execute(parkCommand);
        Assert.assertEquals("Car already parked", resultForDuplicatePark);
    }

    @Test
    public void testLeaveCar(){
        String createParkingLotCommand = "create_parking_lot 6";
        commandProcessor.execute(createParkingLotCommand);
        String parkCommand = "park KA-01-HH-1234 White";
        commandProcessor.execute(parkCommand);
        String leaveCommand = "leave 1";
        String resultForLeave = commandProcessor.execute(leaveCommand);
        Assert.assertEquals("Slot number 1 is free", resultForLeave);
    }

    @Test
    public void testCarsWithColor(){
        String createParkingLotCommand = "create_parking_lot 6";
        commandProcessor.execute(createParkingLotCommand);
        String parkCommand = "park KA-01-HH-1234 White";
        String parkCar2 = "park KA-01-HH-5686 White";
        commandProcessor.execute(parkCommand);
        commandProcessor.execute(parkCar2);
        String getCarsWithColorCommand = "registration_numbers_for_cars_with_colour White";
        String resultForCarsWithColorCommand = commandProcessor.execute(getCarsWithColorCommand );
        Assert.assertEquals("KA-01-HH-1234, KA-01-HH-5686", resultForCarsWithColorCommand);
    }


    @Test
    public void testSlotsWithColor(){
        String createParkingLotCommand = "create_parking_lot 6";
        commandProcessor.execute(createParkingLotCommand);
        String parkCommand = "park KA-01-HH-1234 White";
        String parkCar2 = "park KA-01-HH-5686 White";
        commandProcessor.execute(parkCommand);
        commandProcessor.execute(parkCar2);
        String getCarsWithColorCommand = "slot_numbers_for_cars_with_colour White";
        String resultForCarsWithColorCommand = commandProcessor.execute(getCarsWithColorCommand );
        Assert.assertEquals("1, 2", resultForCarsWithColorCommand);
    }

    @Test
    public void testSlotsWithNumber(){
        String createParkingLotCommand = "create_parking_lot 6";
        commandProcessor.execute(createParkingLotCommand);
        String parkCommand = "park KA-01-HH-1234 White";
        String parkCar2 = "park KA-01-HH-5686 White";
        commandProcessor.execute(parkCommand);
        commandProcessor.execute(parkCar2);
        String getSlotsWithNumberCommand = "slot_number_for_registration_number KA-01-HH-1234";
        String resultForSlotsWithNumberCommand = commandProcessor.execute(getSlotsWithNumberCommand );
        Assert.assertEquals("1", resultForSlotsWithNumberCommand);
    }

}