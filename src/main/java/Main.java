import parser.CommandParser;
import queryprocessor.CommandProcessor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

public class Main {

    private static CommandParser queryParser = new CommandParser();
    private static CommandProcessor queryProcessor = new CommandProcessor();

    public static void main(String[] args) {

        BufferedReader bufferReader = null;
        Scanner scanner = new Scanner(System.in);
        String input;
        try {
            // Interactive input
            if (args.length == 0) {
                System.out.println("Type 'exit' to stop");
                while (true) {
                    try {
                        input = scanner.nextLine();
                        if (input.equalsIgnoreCase("exit")) {
                            break;
                        }
                        String output = executeCommand(input);
                        System.out.println(output);

                    } catch (Exception e) {
                        System.out.println("Input parsing error");
                    }
                }
            }

            // File input/output
            else if (args.length == 1) {
                try {
                    File inputFile = new File(args[0]);
                    bufferReader = new BufferedReader(new FileReader(inputFile));
                    while ((input = bufferReader.readLine()) != null) {

                        String output = executeCommand(input);
                        System.out.println(output);

                    }
                } catch (Exception e) {
                    System.out.println("Invalid command exception");
                }
            } else {
                System.out.println("Provide a proper input");
            }
        } catch (Exception e) {
            System.out.println("Error in parsing input");
        } finally {
            try {
                if (bufferReader != null)
                    bufferReader.close();
            } catch (Exception e) {
                System.out.println("Error with buffer reader");
            }
        }
    }

    // Validate and Execute input command
    private static String executeCommand(String command){

        command = command.trim();
        if (!queryParser.validateQuery(command)){
            return "Error in command : " + command;
        }
        // If command is valid, process it
        String output = queryProcessor.execute(command);

        return output;
    }

}
