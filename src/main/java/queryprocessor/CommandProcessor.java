package queryprocessor;

import models.Car;
import parkinglot.ParkingLot;

import java.util.HashMap;
import java.util.Map;

public class CommandProcessor {

    private static final String CREATE_COMMAND = "create_parking_lot";
    private static final String LEAVE_COMMAND = "leave";
    private static final String PARK_COMMAND = "park";
    private static final String STATUS_COMMAND = "status";
    private static final String COLOR_TO_NUMBER_COMMAND = "registration_numbers_for_cars_with_colour";
    private static final String NUMBER_TO_SLOT_COMMAND = "slot_number_for_registration_number";
    private static final String COLOR_TO_SLOT_COMMAND = "slot_numbers_for_cars_with_colour";

    private ParkingLot parkingLot = null;

    /*
    Executes the command and returns the output to be printed
     */
    public String execute(String query) {

        String[] splitQuery = query.split(" ");
        String queryType = splitQuery[0];

        switch (queryType) {
            case CREATE_COMMAND:
                if (!parkingLotExits()) {
                    // Validation if splitQuery[1] can be converted to Integer without any exception is done in CommandParser
                    int size = Integer.parseInt(splitQuery[1]);
                    parkingLot = new ParkingLot(size);
                    return "Created a parking lot with " + splitQuery[1] + " slots";
                } else {
                    return "Parking lot already exits";
                }

            case PARK_COMMAND:
                if (!parkingLotExits()){
                    return "Parking Lot does not exits";
                }
                String number = splitQuery[1];
                String color = splitQuery[2];
                Car newCar = new Car(number,color);
                return parkingLot.park(newCar);

            case LEAVE_COMMAND:
                if (!parkingLotExits()){
                    return "Parking Lot does not exits";
                }
                String slotNumber = splitQuery[1];
                return parkingLot.leave(slotNumber);


            case STATUS_COMMAND:
                if (!parkingLotExits()){
                    return "Parking Lot does not exits";
                }
                HashMap<Integer, Car> parkedSlots = parkingLot.printStatus();
                // Using StringBuilder as the commands will be one after other and not concurrent
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Slot No.    Registration No    Colour");
                for (Map.Entry<Integer, Car> entry : parkedSlots.entrySet()) {
                    int slot = entry.getKey();
                    Car car = entry.getValue();
                    stringBuilder.append("\n");
                    stringBuilder.append(slot + "           " + car.getNumber() + "      " + car.getColor());
                }
                return stringBuilder.toString();

            case COLOR_TO_NUMBER_COMMAND:
                if (!parkingLotExits()){
                    return "Parking Lot does not exits";
                }
                String colorOfCars = splitQuery[1];
                String numbers = parkingLot.getCarNumbersWithColors(colorOfCars);
                return numbers;

            case COLOR_TO_SLOT_COMMAND:
                if (!parkingLotExits()){
                    return "Parking Lot does not exits";
                }
                String colorOfCar = splitQuery[1];
                String slotNumbers = parkingLot.getCarSlotNumbersWithColors(colorOfCar);
                return slotNumbers;

            case NUMBER_TO_SLOT_COMMAND:
                if (!parkingLotExits()){
                    return "Parking Lot does not exits";
                }
                String registrationNumberOfCar = splitQuery[1];
                String slotNumberFromReg = parkingLot.getCarSlotNumbersWithRegistrationNumber(registrationNumberOfCar);
                return slotNumberFromReg;

            default:
                return "Sorry cannot recognise the command";
        }
    }


    private boolean parkingLotExits() {
        return this.parkingLot != null;
    }
}
