package parkinglot;

import models.Car;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class ParkingLot {

    // we are maintaining a priority queue so that we get the nearest open parking slot in O(log N) time.
    PriorityQueue<Integer> openSlots;
    HashMap<Car, Integer> parkedCars;
    HashMap<Integer, Car> parkedSlots;

    public ParkingLot(int size) {
        this.openSlots = new PriorityQueue<>(size);
        for (int i = 1; i <= size; i++) {
            openSlots.add(i);
        }
        parkedCars = new HashMap<>();
        parkedSlots = new HashMap<>();
    }

    // Park method can be made thread-safe by using proper locks if we get concurrent queries.
    public String park(Car car) {

        if (openSlots.size() == 0) {
            return "Sorry, parking lot is full";
        }
        if (parkedCars.containsKey(car)) {
            return "Car already parked";
        }
        int nearestOpenSlot = openSlots.poll();
        openSlots.remove(nearestOpenSlot);
        parkedSlots.put(nearestOpenSlot, car);
        parkedCars.put(car, nearestOpenSlot);
        return "Allocated slot number: " + nearestOpenSlot;
    }


    // Leave method can also be made thread safe once we start supporting concurrent queries
    public String leave(String slotNumber) {
        int slot = Integer.parseInt(slotNumber);
        if (parkedSlots.containsKey(slot)) {
            Car carToRemove = parkedSlots.get(slot);
            parkedSlots.remove(slot);
            parkedCars.remove(carToRemove);
            openSlots.add(slot);
            return "Slot number " + slotNumber + " is free";
        } else {
            return "Car is not in the parking lot";
        }
    }

    public HashMap<Integer, Car> printStatus() {
        return parkedSlots;
    }

    public String getCarNumbersWithColors(String colorOfCars) {

        String carNumbers = "";

        for (Map.Entry<Integer, Car> entry : parkedSlots.entrySet()) {
            Car car = entry.getValue();
            if (colorOfCars.equalsIgnoreCase(car.getColor())){
                carNumbers += car.getNumber() + ", ";
            }
        }
        if (carNumbers.equalsIgnoreCase("")){
            return "No car with color " + colorOfCars;
        }
        int lastIndex = carNumbers.lastIndexOf(",");
        carNumbers = carNumbers.substring(0,lastIndex);
        return carNumbers;

    }

    public String getCarSlotNumbersWithColors(String colorOfCars) {

        String carSlotNumbers = "";

        for (Map.Entry<Integer, Car> entry : parkedSlots.entrySet()) {
            Car car = entry.getValue();
            if (colorOfCars.equalsIgnoreCase(car.getColor())){
                carSlotNumbers += entry.getKey() + ", ";
            }
        }
        if (carSlotNumbers.equalsIgnoreCase("")){
            return "No car with color " + colorOfCars;
        }
        int lastIndex = carSlotNumbers.lastIndexOf(",");
        carSlotNumbers = carSlotNumbers.substring(0,lastIndex);
        return carSlotNumbers;

    }


    public String getCarSlotNumbersWithRegistrationNumber(String registrationNumber) {
        String slotNumber = "";

        for (Map.Entry<Integer, Car> entry : parkedSlots.entrySet()) {
            Car car = entry.getValue();
            if (registrationNumber.equalsIgnoreCase(car.getNumber())){
                slotNumber = entry.getKey().toString();
                // need to add break here
            }
        }
        if (slotNumber.equalsIgnoreCase("")){
            return "Not found";
        }
        return slotNumber;

    }
}
