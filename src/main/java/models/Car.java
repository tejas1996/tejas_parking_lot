package models;

import java.util.Objects;

public class Car {
    private String number;
    private String color;

    public Car(String number, String color) {
        this.number = number;
        this.color = color;
    }

    public String getNumber() {
        return number;
    }

    public String getColor() {
        return color;
    }

    @Override
    public int hashCode() {
        return number.hashCode() + color.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(number, car.number) &&
                Objects.equals(color, car.color);
    }

}

