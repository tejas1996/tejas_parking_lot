package parser;

public class CommandParser {


    private static final String CREATE_COMMAND = "create_parking_lot";
    private static final String LEAVE_COMMAND = "leave";
    private static final String PARK_COMMAND = "park";
    private static final String STATUS_COMMAND = "status";
    private static final String COLOR_TO_NUMBER_COMMAND = "registration_numbers_for_cars_with_colour";
    private static final String NUMBER_TO_SLOT_COMMAND = "slot_number_for_registration_number";
    private static final String COLOR_TO_SLOT_COMMAND = "slot_numbers_for_cars_with_colour";

    /*
    Validates the input to check for give set of commands
    Also validates the other input associated with it (eg : "leave toyota" is a wrong input as we expect a parking slot instead of car name)
     */
    public boolean validateQuery(String query) {

        boolean valid = true;
        query = query.trim();
        String[] inputs = query.split(" ");
        if (inputs.length == 0){
            // no input
            return false;
        }

        String commandType = inputs[0];

        switch (commandType) {
            case CREATE_COMMAND:
            case LEAVE_COMMAND:
                if (inputs.length != 2) {
                    valid = false;
                }
                try {
                    Integer.parseInt(inputs[1]);
                } catch (NumberFormatException e){
                    valid = false;
                }
                break;
            case PARK_COMMAND:
                if (inputs.length != 3) {
                    valid = false;
                }
                break;

            case STATUS_COMMAND:
                if (inputs.length != 1) {
                    valid = false;
                }
                break;

            case COLOR_TO_NUMBER_COMMAND:
            case NUMBER_TO_SLOT_COMMAND:
            case COLOR_TO_SLOT_COMMAND:
                if (inputs.length != 2) {
                    valid = false;
                }
                break;

            default:
                valid = false;
        }

        return valid;
    }
}
